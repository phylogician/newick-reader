import * as fs from 'fs'
import * as newick from '../src/newick';
import { expect } from 'chai'

const countLeafs = (tree: newick.ITree, count = 0) => {
    let newCount = count
    if (tree.children.length === 0) {
        newCount++
    }
    else {
        tree.children.forEach((child) => {
            newCount += countLeafs(child)
        })
    }
    return newCount
}

describe('newick', () => {
    describe('read', () => {
        it('countLeafs should work', () => {
            const tree:string = '(A,(B,C));';
            const expectedCount = 3
            const testTree = newick.read(tree);
            const numberOfLeafs = countLeafs(testTree)
            expect(numberOfLeafs).eql(expectedCount)
        })
        it('it should work with simple trees', () => {
            const tree:string = '(A,(B,C));';
            const expectedTree = {
                branchLength: null,
                children: [
                    {
                        branchLength: null,
                        children: [],
                        name: 'A',
                    },
                    {
                        branchLength: null,
                        children: [
                            {
                                branchLength: null,
                                children: [],
                                name: 'B',
                            },
                            {
                                branchLength: null,
                                children: [],
                                name: 'C',
                            }
                        ],
                        name: '',
                    },
                ],
                name: '',
            }
            const testTree = newick.read(tree);
            expect(testTree).eql(expectedTree)
        })
        it('it should work with more complex trees', () => {
            const tree:string = '(A,(B,C),(D,E,(F,G)));';
            const expectedTree:newick.ITree = {
                branchLength: null,
                children: [
                    {
                        branchLength: null,
                        children: [],
                        name: 'A'
                    },
                    {
                        branchLength: null,
                        children: [
                            {
                                branchLength: null,
                                children: [],
                                name: 'B'
                            },
                            {
                                branchLength: null,
                                children: [],
                                name: 'C'
                            }
                        ],
                        name: '',
                    },
                    {
                        branchLength: null,
                        children: [
                            {
                                branchLength: null,
                                children: [],
                                name: 'D'
                            },
                            {
                                branchLength: null,
                                children: [],
                                name: 'E'
                            },
                            {
                                branchLength: null,
                                children: [
                                    {
                                        branchLength: null,
                                        children: [],
                                        name: 'F'
                                    },
                                    {
                                        branchLength: null,
                                        children: [],
                                        name: 'G'
                                    }
                                ],
                                name: '',
                            },
                        ],
                        name: '',
                    },
                ],
                name: '',
            }
            const testTree = newick.read(tree);
            expect(testTree).eql(expectedTree)
        })
        it('it should work with trees with branch lengths', () => {
            const tree:string = '(A:2,(B:1,C:1):3);';
            const expectedTree = {
                branchLength: null,
                children: [
                    {
                        branchLength: 2,
                        children: [],
                        name: 'A',
                    },
                    {
                        branchLength: 3,
                        children: [
                            {
                                branchLength: 1,
                                children: [],
                                name: 'B'
                            },
                            {
                                branchLength: 1,
                                children: [],
                                name: 'C'
                            }
                        ],
                        name: '',
                    },
                ],
                name: '',
            }
            const testTree = newick.read(tree);
            expect(testTree).eql(expectedTree)
        })
        it('it should work with real life trees (GTDB)', () => {
            const tree:string = "(((UBA9212:0.00242,UBA9293:0.00248)'100:f__UBA9212; g__UBA9212; s__UBA9212 sp1':0.38622,GB_GCA_002838935.1:0.30606)'100:o__UBA9212':0.05663)100:0.05346" // fs.readFileSync('./tests/ar.tree').toString()
            const expectedTree = {
                "branchLength": 0.05346,
                "bs": 100,
                "children": [
                 {
                  "branchLength": 0.05663,
                  "bs": 100,
                  "children": [
                   {
                    "branchLength": 0.38622,
                    "bs": 100,
                    "children": [
                     {
                      "branchLength": 0.00242,
                      "children": [],
                      "name": "UBA9212"
                     },
                     {
                      "branchLength": 0.00248,
                      "children": [],
                      "name": "UBA9293"
                     }
                    ],
                    "name": "f__UBA9212; g__UBA9212; s__UBA9212 sp1"
                   },
                   {
                    "branchLength": 0.30606,
                    "children": [],
                    "name": "GB_GCA_002838935.1"
                   }
                  ],
                  "name": "o__UBA9212",
                 }
                ],
                "name": "",
            }
            const testTree = newick.read(tree);
            expect(testTree).eql(expectedTree)
        })
        it('it should work with real life medium size trees (Ar - GTDB)', () => {
            const tree:string = fs.readFileSync('./test/ar.tree').toString()
            const expectedNumberOfLeafs = 1248
            const testTree = newick.read(tree);
            const numberOfLeafs = countLeafs(testTree)
            expect(numberOfLeafs).eql(expectedNumberOfLeafs)
        })
        it('it should work with real life large size trees (Bac - GTDB)', () => {
            const tree:string = fs.readFileSync('./test/bac.tree').toString()
            const expectedNumberOfLeafs = 23458
            const testTree = newick.read(tree);
            const numberOfLeafs = countLeafs(testTree)
            expect(numberOfLeafs).eql(expectedNumberOfLeafs)
        })
        it('it should work with trees with branch lengths and internal labels', () => {
            const tree:string = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedTree = {
                "branchLength": null,
                "children": [
                 {
                  "branchLength": 4,
                  "children": [
                   {
                    "branchLength": 2,
                    "children": [],
                    "name": "A2lice"
                   },
                   {
                    "branchLength": 3,
                    "children": [],
                    "name": "D3ebbie"
                   }
                  ],
                  "name": "1N",
                 },
                 {
                  "branchLength": 3,
                  "children": [
                   {
                    "branchLength": 2,
                    "children": [
                     {
                      "branchLength": 3,
                      "children": [],
                      "name": "B6ob"
                     },
                     {
                      "branchLength": 4,
                      "children": [],
                      "name": "C7harlie"
                     },
                     {
                      "branchLength": 3,
                      "children": [
                       {
                        "branchLength": 2,
                        "children": [
                         {
                          "branchLength": 3,
                          "children": [],
                          "name": "E10llen"
                         },
                         {
                          "branchLength": 4,
                          "children": [],
                          "name": "F11elix"
                         }
                        ],
                        "name": "N9"
                       },
                       {
                        "branchLength": 5,
                        "children": [],
                        "name": "G12il"
                       }
                      ],
                      "name": "N8"
                     }
                    ],
                    "name": "N5"
                   },
                   {
                    "branchLength": 5,
                    "children": [],
                    "name": "I13lly"
                   }
                  ],
                  "name": "N4"
                 }
                ],
                "name": "0N",
               }
            const testTree = newick.read(tree);
            expect(testTree).eql(expectedTree)
        })
        
    })
    describe('write', () => {
        it('it should work with simple trees', () => {
            const treeJson:newick.ITree = {
                branchLength: null,
                children: [
                    {
                        branchLength: null,
                        children: [],
                        name: 'A'
                    },
                    {
                        branchLength: null,
                        children: [
                            {
                                branchLength: null,
                                children: [],
                                name: 'B'
                            },
                            {
                                branchLength: null,
                                children: [],
                                name: 'C'
                            }
                        ],
                        name: '',
                    },
                ],
                name: '',
            }
            const expectedNewick ='(A,(B,C));';
            const testNewick = newick.write(treeJson);
            expect(testNewick).eql(expectedNewick)
        })
        it('it should work with trees with branch lengths', () => {
            const treeJson = {
                branchLength: null,
                children: [
                    {
                        branchLength: 2,
                        children: [],
                        name: 'A',
                    },
                    {
                        branchLength: 3,
                        children: [
                            {
                                branchLength: 1,
                                children: [],
                                name: 'B'
                            },
                            {
                                branchLength: 1,
                                children: [],
                                name: 'C'
                            }
                        ],
                        name: '',
                    },
                ],
                name: '',
            }
            const expectedNewick:string = '(A:2,(B:1,C:1):3);';
            const testNewick = newick.write(treeJson as newick.ITree);
            expect(testNewick).eql(expectedNewick)
        })
    })
})
