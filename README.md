# newick-reader

[![pipeline status](https://gitlab.com/phylogician/newick-reader/badges/master/pipeline.svg)](https://gitlab.com/phylogician/newick-reader/commits/master) [![coverage report](https://gitlab.com/phylogician/newick-reader/badges/master/coverage.svg)](https://gitlab.com/phylogician/newick-reader/commits/master)

TypeScript support for newick library reader based on Miguel Pignatelli 2014, based on Jason Davies 2010.

## Install

```bash
npm install newick-reader
```

## Usage

To parse a newick string into an Object:

```typescript
import * as newickReader from 'newick-reader';

const newickString:string = '(A,(B,C));';
const treeObject = newickReader.parseNewick(newickString);

/*
treeObject =
{
    children: [
        {
            name: 'A'
        },
        {
            children: [
                {
                    name: 'B'
                },
                {
                    name: 'C'
                }
            ],
            name: '',
        },
    ],
    name: '',
}
*/
```

Although this is supose to be a reader, it also comes with a writer:

```typescript
import * as newickReader from 'newick-reader';

const treeObject:object = {
    children: [
        {
            name: 'A'
        },
        {
            children: [
                {
                    name: 'B'
                },
                {
                    name: 'C'
                }
            ],
            name: '',
        },
    ],
    name: '',
}

const  newickString = newickReader.parseJson(treeObject);

/*
newickString = '(A,(B,C));';
*/
```
